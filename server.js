const express = require('express');
const pdfHandler  = require('./PdfHandler.js');
const app = express();
const bodyParser = require('body-parser');
const config = require('./config')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}))

app.post('/mergeFilesIntoPdf', function (req, res) {
    new pdfHandler().mergeFiles(req.body)
    .then((data)=> {
        res.send({
            success: true,
            files: data
        })
    })
    .catch((err)=> {
        console.log(err)
        res.send({
            success: false,
            errorMessage: err.message
        })
    })
});

app.listen(config.PORT, ()=> {
    console.log("Listening on port: " + config.PORT)
});