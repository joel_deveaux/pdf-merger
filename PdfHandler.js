const PDFDocument = require('pdfkit');
const PDFMerger = require('pdf-merger-js');
const PDFMerge = require('pdf-merge');
const qpdf = require('node-qpdf');
const jimp = require('jimp');
const ConvertTiff = require('tiff-to-png');
const { exec, chdir } = require('child_process');
const path = require('path')


const fs = require('fs');

module.exports = function () {

    //Main
    async function mergeFiles(files) {
        try {

            const tiffs = await convertTifToPng(files.images);
            const addImageResponse = await addImages(files.images.concat(tiffs));

            await new Promise((resolve) => setTimeout(resolve, 500));


            const pdfCount = await concatPdfs(files.pdfs, addImageResponse.fileName);

            console.log(`PDF ${addImageResponse.fileName} generated successfully at folder '/outputs'`);

            cleanTempFiles();

            return {
                'images': addImageResponse.imgCount,
                'pdfs': pdfCount
            }

        } catch (err) {
            return err.message;
        }
    }

    //Funciones Internas
    async function concatPdfs(pdfs, fileName) {
        let count = 0;

        const merger = new PDFMerger();

        for (let i = 0; i < pdfs.length; i++) {
            let pdf = pdfs[i]
            let newPdf = await decryptPdf(pdf);

            console.log(`Merging ${pdf.toString()}....`)
            try {
                merger.add(newPdf.toString())
            } catch (err) {
                throw new Error(err)
            }
            console.log("PDF '" + pdf.toString() + "' was merged to " + fileName);
            count++;
        };

        merger.add('./tmp/' + fileName);

        await merger.save(`./outputs/${fileName}`);

        return count;
    };

    async function convertTifToPng(images) {

        const fsPromises = fs.promises;
        var options = {
            logLevel: 1
        };

        const tiffImages = await images.filter(image => {
            return image.indexOf(".tif") !== -1;
        })

        if (!tiffImages.length)
            return [];

        var filePaths = tiffImages[0].replace(/\\/g, '/').split('/');
        var fileName = filePaths[filePaths.length - 1].split('.')[0];
        var extension = '.' + filePaths[filePaths.length - 1].split('.')[1];

        const tiffImgNewUrl = await tiffImages.map(img => {
            const response = fsPromises.copyFile(img, 'tmp/' + fileName + extension);
            return 'tmp/' + fileName + extension
        })
        var converter = new ConvertTiff(options);

        try {
            const response = await converter.convertArray(tiffImgNewUrl, 'tmp');


            if (!response.errors.length) {
                const pngFiles = await fsPromises.readdir('tmp/' + fileName);
                return pngFiles.map(file => {
                    return 'tmp/' + fileName + '/' + file;
                })
            } else {
                console.log(response.errors)
                return [];
            }
        } catch (e) {
            console.log(e)
            return [];
        }
    }


    async function addImages(images) {
        let imageList = images;
        let count = 0;

        const fileName = `${new Date().getTime()}.pdf`;
        const doc = new PDFDocument();

        doc.pipe(fs.createWriteStream(`./tmp/${fileName}`));

        for (let i = 0; i < imageList.length; i++) {

            let image = imageList[i];
            let imageUrl = image;

            if (image.indexOf('.bmp') != -1) {
                imageUrl = await convertBmpToImage(image);
            };

            if (image.indexOf('.tif') != -1) {
                continue;
            };
            if (imageUrl) {
                if (i == 0) {
                    doc.image(imageUrl || image, {
                        fit: [500, 400],
                        align: 'center',
                        valign: 'center'
                    });
                } else {
                    doc.addPage()
                        .image(imageUrl || image, {
                            fit: [500, 400],
                            align: 'center',
                            valign: 'center'
                        });
                };

                console.log("image " + image + " was printed");
                count++;
            }

        }

        doc.end();

        return {
            imgCount: count,
            fileName: fileName
        };

    };

    function convertBmpToImage(image) {
        return new Promise((resolve, reject) => {
            try {
                console.log(image)
                const filePath = image.replace(/\\/g, '/').split('/');
                const fileName = filePath[filePath.length - 1];
                const imgUrl = 'tmp/' + fileName.replace('.bmp', '.jpg');
                jimp.read(image, function (err, imgBmp) {
                    if (err) reject(err);

                    imgBmp.write(imgUrl, function (err) {
                        if (err) reject(err);
                        resolve(imgUrl);
                    });
                });
            } catch (e) {
                reject(e)
            }

        })
    };

    function decryptPdf(pdf) {
        return new Promise((resolve, reject) => {
            var filePaths = pdf.replace(/\\/g, '/').split('/');
            var fileName = filePaths[filePaths.length - 1];

            let newPdf = __dirname + '/tmp/' + fileName;

            var comand = 'qpdf --decrypt "' + pdf + '" "' + newPdf + '"';

            exec(comand, {cwd: 'bin'}, function (err) {
                if (err) reject(err);
                resolve(newPdf)
            });
        })
    }

    function cleanTempFiles() {

        const directory = './tmp';

        fs.readdir(directory, (err, files) => {
            if (err) throw err;

            for (const file of files) {
                if (file.includes(".")) {
                    fs.unlink(path.join(directory, file), err => {
                        if (err) throw err;
                    });
                } else {
                    fs.readdir(`${directory}/${file}`, (err, sdFiles) => {
                        if (err) throw err;

                        for (const f of sdFiles) {
                            fs.unlink(path.join(`${directory}/${file}`, f), err => {
                                if (err) throw err;
                            });
                            console.log("file " + f + " Removed")
                        }

                        fs.rmdir(path.join(directory, file), err => {
                            if (err) throw err;
                        });
                    });

                }
                console.log("file " + file + " Removed")
            }
        });

    };

    //No se esta utilizando
    function convertTifToPdf(image) {
        return new Promise((resolve, reject) => {
            tiff2pdf(image, '/tmp', function (result) {
                resolve(result);
            });

        })
    };


    //No se esta utilizando
    function convertTifToImage(image) {
        return new Promise((resolve, reject) => {
            const filePath = image.split('/');
            const fileName = filePath[filePath.length - 1];
            const imgUrl = 'images/' + fileName.replace('.tif', '.jpg');
            gm(image).write(imgUrl, function (err) {
                if (err) reject(err);

                resolve(imgUrl)
            });
        })

    }

    return {
        mergeFiles,
    };

}